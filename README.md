# familog frontend

This application is a frontend for [familog](http://familog.co.jp/).

## How to develop

This application is developed with JavaScript and Vuejs. Please make sure you need to run `yarn install` before developing this app.

```bash
yarn install
```

If you want to deploy this app in development mode, `vue-cli` can handle that with the following command.

```bash
yarn serve
```

If you want to run tests, `vue-cli` also handle that, too. As to tests, this app has only snapshot tests. In initial development, this app is developed without TDD/BDD.

```bash
yarn test:unit
```

## How to build

1. Build this application with `vue-cli`
2. Upload that to S3
3. Invalidate cloudfront cache

### production

```bash
yarn build
cd dist
aws s3 sync ./ s3://app.familog.co.jp --delete --cache-control max-age=0,no-cache,no-store --profile familog-admin
aws cloudfront create-invalidation --distribution-id $CLOUDFRONT_DISTRIBUTION_ID --paths "/*" --profile familog-admin
```

### development

```bash
yarn build
cd dist
aws s3 sync ./ s3://dev-app.familog.co.jp --delete --cache-control max-age=0,no-cache,no-store --profile familog-admin
aws cloudfront create-invalidation --distribution-id $CLOUDFRONT_DISTRIBUTION_ID --paths "/*" --profile familog-admin
```

## Tech stack

This application is using...

1. Vuejs
2. Vue-Router
3. AWS-Amplify
4. Jest for tests
5. Bootstrap for some UIs
6. Pure CSS for other UIs

## Backend

Backend has two endpoints, which are `posts` and `users`. They are just REST APIs. If you want to know them in detail, please check the code base.
