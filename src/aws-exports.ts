import * as dotenv from 'dotenv';
dotenv.config();

export default {
  Auth: {
    mandatorySignIn: true,
    region: process.env.VUE_APP_REGION,
    userPoolId: process.env.VUE_APP_USERPOOLID,
    identityPoolId: process.env.VUE_APP_IDENTITYPOOLID,
    userPoolWebClientId: process.env.VUE_APP_USERPOOLWEBCLIENTID
  },
  API: {
    endpoints: [
      {
        name: 'ampFamilog',
        endpoint: process.env.VUE_APP_ENDPOINT,
        region: process.env.VUE_APP_REGION
      }
    ]
  }
};
