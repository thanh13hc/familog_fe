import moment from 'moment';
import { DATE_FORMAT_DISPLAY } from '@/constants/lang';

export const dateFormat = {
  convertTimestampToDate(time) {
    if (time) {
      return moment(time).format(DATE_FORMAT_DISPLAY);
    }
    return '';
  }
};
