export const shortenText = (text: string, maxBytes: number): string => {
  const textBytes = encodeURIComponent(text).replace(/%../g, 'x').length;
  let result = text;
  if (textBytes > maxBytes) {
    const subtractedLengthfromBytes = textBytes - text.length;
    if (subtractedLengthfromBytes > 0) {
      result = text.slice(0, maxBytes / 2 - text.length) + '..';
    } else {
      result = text.slice(0, maxBytes - textBytes) + '..';
    }
  }
  return result;
};
