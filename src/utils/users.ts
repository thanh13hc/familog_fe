import { API } from 'aws-amplify';
import { UserType } from '@/types/UserTypes';

export const getUser = async (
  searchWithID: string,
  searchKey: string
): Promise<UserType> => {
  if (!searchKey) {
    throw new TypeError('searchWithKey cannot be null');
  }
  let path = '';
  if (searchWithID) {
    path = `/users/id/${searchKey}`;
  } else {
    path = `/users/phone/${searchKey}`;
  }

  try {
    const result = await API.get('ampFamilog', path, { response: true });
    return result.data;
  } catch (error) {
    if (error.response.data && error.response.data.message) {
      throw new Error(error.response.data.message);
    }
    throw error;
  }
};

export const getLocalUserInfo = field => {
  const userInfo = JSON.parse(localStorage.getItem('user'));

  if (field) {
    return userInfo[field];
  } else {
    return userInfo;
  }
};

export const createUser = async (
  userTobeCreated: UserType
): Promise<UserType> => {
  if (!userTobeCreated) {
    throw new TypeError('userTobeCreated cannot be null');
  }
  const params = {
    response: true,
    body: userTobeCreated,
    headers: {}
  };

  try {
    const result = await API.post('ampFamilog', '/users', params);
    return result.data;
  } catch (error) {
    if (error.response.data && error.response.data.message) {
      throw new Error(error.response.data.message);
    }
    throw error;
  }
};

export const updateUser = async (
  userID: string,
  userTobeUpdated: Partial<UserType>
): Promise<UserType> => {
  if (!userID || !userTobeUpdated) {
    throw new TypeError('userID or userTobeUpdated cannot be null');
  }
  try {
    const result = await API.put('ampFamilog', `/users/${userID}`, {
      response: true,
      body: userTobeUpdated
    });
    return result.data;
  } catch (error) {
    if (error.response.data && error.response.data.message) {
      throw new Error(error.response.data.message);
    }
    throw error;
  }
};
