export const formatDateWithPeriod = (dateNumber: number): string => {
  const extractedDate = new Date(dateNumber).toISOString().slice(0, 10);
  const formatChanged = extractedDate.replace(/-/g, '.');
  return formatChanged;
};

export const formatDateWithHyphen = (dateNumber: number): string => {
  try {
    return new Date(dateNumber).toISOString().slice(0, 10);
  } catch (error) {
    if (error instanceof RangeError) {
      throw new Error(`Empty date value: ${error}`);
    } else {
      throw error;
    }
  }
};
