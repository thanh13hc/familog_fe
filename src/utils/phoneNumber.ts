export const removeFirstZero = (phoneNumber: string): string => {
  if (!phoneNumber.startsWith('0')) {
    return phoneNumber;
  }
  return phoneNumber.slice(1);
};
