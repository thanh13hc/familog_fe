export const readImage = async (
  file: Blob
): Promise<string | ArrayBuffer | null> =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = event => {
      resolve(event.target?.result);
    };
    reader.onerror = () => {
      reject(new Error('Failed to read an image'));
    };
    reader.readAsDataURL(file);
  });
