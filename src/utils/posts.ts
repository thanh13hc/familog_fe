import { API } from 'aws-amplify';
import { PostType } from '@/types/PostTypes';

export const createPost = async (post: PostType): Promise<PostType> => {
  if (!post) {
    throw new TypeError('post cannot be null');
  }
  try {
    const result = await API.post('ampFamilog', '/posts', {
      response: true,
      body: post
    });

    return result.data;
  } catch (error) {
    if (error.response.data && error.response.data.message) {
      throw new Error(error.response.data.message);
    }
    throw error;
  }
};

export const getPost = async (postID: string): Promise<PostType> => {
  if (!postID) {
    throw new Error('postID or post cannot be null');
  }
  try {
    const result = await API.get('ampFamilog', `/posts/${postID}`, {
      response: true
    });
    return result.data;
  } catch (error) {
    if (error.response.data && error.response.data.message) {
      throw new Error(error.response.data.message);
    }
    throw error;
  }
};

export const getPosts = async (
  userID: string,
  refererID: string
): Promise<PostType[]> => {
  if (!userID) {
    throw new TypeError('userID cannot be null');
  }
  if (userID === refererID) {
    const path = '/posts/users';
    const myInit = {
      response: true,
      headers: {},
      queryStringParameters: {
        userID: userID
      }
    };
    try {
      const result = await API.get('ampFamilog', path, myInit);

      return result.data.data;
    } catch (error) {
      if (error.response.status === 404) {
        return [];
      }
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  }

  const path = '/posts/shared';
  const myInit = {
    headers: {},
    response: true,
    queryStringParameters: {
      userID,
      refererID
    }
  };
  try {
    const result = await API.get('ampFamilog', path, myInit);
    return result.data.data;
  } catch (error) {
    if (error.response.status === 404) {
      return [];
    }
    if (error.response.data && error.response.data.message) {
      throw new Error(error.response.data.message);
    }
    throw error;
  }
};

export const deletePost = async (postID: string): Promise<void> => {
  if (!postID) {
    throw new TypeError('postID cannot be null');
  }
  try {
    await API.del('ampFamilog', `/posts/${postID}`, {
      headers: {},
      body: { postID: postID },
      response: true
    });
  } catch (error) {
    if (error.response.data && error.response.data.message) {
      throw new Error(error.response.data.message);
    }
    throw error;
  }
};

export const updatePost = async (
  postID: string,
  post: PostType
): Promise<PostType> => {
  if (!postID || !post) {
    throw new TypeError('postID or post cannot be null');
  }
  try {
    const result = await API.put('ampFamilog', `/posts/${postID}`, {
      response: true,
      body: post
    });
    return result.data;
  } catch (error) {
    if (error.response.data && error.response.data.message) {
      throw new Error(error.response.data.message);
    }
    throw error;
  }
};
