import { API } from 'aws-amplify';

export default {
  async getAllData({ commit }, payload) {
    const userID = payload;
    if (!userID) {
      throw new TypeError('userID cannot be null');
    }

    try {
      const result = await API.get(
        'ampFamilog',
        `/users/info?userID=${userID}`,
        { response: true }
      );
      if (result.data) {
        await commit('GET_ALL_DATA', result.data);
      }
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },
  /*
   * Hospital
   */
  async createHospital({ commit }, payload) {
    const hospital = payload;
    if (!hospital) {
      throw new TypeError('hospital cannot be null');
    }

    try {
      const result = await API.post('ampFamilog', '/hospitals', {
        response: true,
        body: hospital
      });

      if (result.data) {
        const data = { action: 'ADD_HOSPITAL', returnData: result.data };
        await commit('HOSPITAL_MUTATION', data);
      }
      return result.data;
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  async getHospital(payload) {
    const { hospitalID } = payload;
    if (!hospitalID) {
      throw new Error('hospitalID or hospital cannot be null');
    }
    try {
      const result = await API.get('ampFamilog', `/hospitals/${hospitalID}`, {
        response: true
      });
      return result.data;
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  async deleteHospital({ commit }, payload) {
    const hospitalID = payload;
    if (!hospitalID) {
      throw new TypeError('hospitalID cannot be null');
    }
    try {
      const result = await API.del('ampFamilog', `/hospitals/${hospitalID}`, {
        headers: {},
        body: { hospitalID: hospitalID },
        response: true
      });

      if (result) {
        const data = { action: 'DELETE_HOSPITAL', returnData: result.data };
        await commit('HOSPITAL_MUTATION', data);
      }
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  async updateHospital({ commit }, payload) {
    const { hospitalID, hospital } = payload;

    if (!hospitalID || !hospital) {
      throw new TypeError('hospitalID or hospitalID cannot be null');
    }
    try {
      const result = await API.put('ampFamilog', `/hospitals/${hospitalID}`, {
        response: true,
        body: hospital
      });
      if (result.data) {
        const data = { action: 'UPDATE_HOSPITAL', returnData: result.data };
        await commit('HOSPITAL_MUTATION', data);
      }
      return result.data;
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  /*
   * Insurance note
   */
  async createInsuranceNote({ commit }, payload) {
    const insuranceNote = payload;

    if (!insuranceNote) {
      throw new TypeError('Insurance Note cannot be null');
    }

    try {
      const result = await API.post('ampFamilog', '/insurancenotes', {
        response: true,
        body: insuranceNote
      });

      if (result.data) {
        const data = { action: 'ADD_INSURANCENOTE', returnData: result.data };
        await commit('INSURANCENOTE_MUTATION', data);
      }
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  async deleteInsuranceNote({ commit }, payload) {
    const insuranceNoteID = payload;

    if (!insuranceNoteID) {
      throw new TypeError('Insurance Note cannot be null');
    }

    try {
      const result = await API.del(
        'ampFamilog',
        `/insurancenotes/${insuranceNoteID}`,
        {
          headers: {},
          body: { insuranceNoteID: insuranceNoteID },
          response: true
        }
      );

      if (result.data) {
        const data = {
          action: 'DELETE_INSURANCENOTE',
          returnData: result.data
        };
        await commit('INSURANCENOTE_MUTATION', data);
      }
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  async updateInsuranceNote({ commit }, payload) {
    const { insuranceNoteID, insuranceNote } = payload;
    if (!insuranceNoteID || !insuranceNote) {
      throw new TypeError('insuranceNoteID or insuranceNote cannot be null');
    }
    try {
      const result = await API.put(
        'ampFamilog',
        `/insurancenotes/${insuranceNoteID}`,
        {
          response: true,
          body: insuranceNote
        }
      );
      if (result.data) {
        const data = {
          action: 'UPDATE_INSURANCENOTE',
          returnData: result.data
        };
        await commit('INSURANCENOTE_MUTATION', data);
      }
      return result.data;
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  /*
   *  HealthTraining
   */
  async createHealthTraining({ commit }, payload) {
    const healthTraining = payload;
    if (!healthTraining) {
      throw new TypeError('healthTraining cannot be null');
    }

    try {
      const result = await API.post('ampFamilog', '/healthtrainings', {
        response: true,
        body: healthTraining
      });

      if (result.data) {
        const data = { action: 'ADD_HEALTHTRAINING', returnData: result.data };
        await commit('HEALTHTRAINING_MUTATION', data);
      }
      return result.data;
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  async getHealthTraining(payload) {
    const { healthTrainingID } = payload;
    if (!healthTrainingID) {
      throw new Error('healthTrainingID cannot be null');
    }
    try {
      const result = await API.get(
        'ampFamilog',
        `/healthtrainings/${healthTrainingID}`,
        {
          response: true
        }
      );
      return result.data;
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  async deleteHealthTraining({ commit }, payload) {
    const healthTrainingID = payload;
    if (!healthTrainingID) {
      throw new TypeError('healthTrainingID cannot be null');
    }
    try {
      const result = await API.del(
        'ampFamilog',
        `/healthtrainings/${healthTrainingID}`,
        {
          headers: {},
          body: { healthTrainingID: healthTrainingID },
          response: true
        }
      );

      if (result) {
        const data = {
          action: 'DELETE_HEALTHTRAINING',
          returnData: result.data
        };
        await commit('HEALTHTRAINING_MUTATION', data);
      }
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  async updateHealthTraining({ commit }, payload) {
    const { healthTrainingID, healthTraining } = payload;

    if (!healthTrainingID || !healthTraining) {
      throw new TypeError('healthTraining or healthTrainingID cannot be null');
    }
    try {
      const result = await API.put(
        'ampFamilog',
        `/healthtrainings/${healthTrainingID}`,
        {
          response: true,
          body: healthTraining
        }
      );
      if (result.data) {
        const data = {
          action: 'UPDATE_HEALTHTRAINING',
          returnData: result.data
        };
        await commit('HEALTHTRAINING_MUTATION', data);
      }
      return result.data;
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  /*
    Examination Schedule 
  */
  async createExaminationSchedule({ commit }, payload) {
    const examinationSchedule = payload;

    if (!examinationSchedule) {
      throw new TypeError('examinationSchedule cannot be null');
    }
    try {
      const result = await API.post('ampFamilog', '/examinationschedules', {
        response: true,
        body: examinationSchedule
      });
      if (result.data) {
        const data = {
          action: 'ADD_EXAMINATIONSCHEDULE',
          returnData: result.data
        };
        await commit('EXAMINATIONSCHEDULE_MUTATION', data);
      }
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  async deleteExaminationSchedule({ commit }, payload) {
    const examinationScheduleID = payload;
    if (!examinationScheduleID) {
      throw new TypeError('examinationScheduleID cannot be null');
    }
    try {
      const result = await API.del(
        'ampFamilog',
        `/examinationschedules/${examinationScheduleID}`,
        {
          headers: {},
          body: { examinationScheduleID: examinationScheduleID },
          response: true
        }
      );

      if (result) {
        const data = {
          action: 'DELETE_EXAMINATIONSCHEDULE',
          returnData: result.data
        };
        await commit('EXAMINATIONSCHEDULE_MUTATION', data);
      }
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  async updateExaminationSchedule({ commit }, payload) {
    const { examinationScheduleID, examinationSchedule } = payload;
    if (!examinationScheduleID || !examinationSchedule) {
      throw new TypeError(
        'examinationSchedule or examinationSchedule cannot be null'
      );
    }
    try {
      const result = await API.put(
        'ampFamilog',
        `/examinationschedules/${examinationScheduleID}`,
        {
          response: true,
          body: examinationSchedule
        }
      );
      if (result.data) {
        const data = {
          action: 'UPDATE_EXAMINATIONSCHEDULE',
          returnData: result.data
        };
        await commit('EXAMINATIONSCHEDULE_MUTATION', data);
      }
      return result.data;
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  /*
    Examination Schedule 
  */
  async createBodyMeasurement({ commit }, payload) {
    const bodyMeasurement = payload;

    if (!bodyMeasurement) {
      throw new TypeError('bodyMeasurement cannot be null');
    }
    try {
      const result = await API.post('ampFamilog', '/bodymeasurements', {
        response: true,
        body: bodyMeasurement
      });
      if (result.data) {
        const data = {
          action: 'ADD_BODYMEASUREMENT',
          returnData: result.data
        };
        await commit('BODYMEASUREMENT_MUTATION', data);
      }
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  async deleteBodyMeasurement({ commit }, payload) {
    const bodyMeasurementID = payload;
    if (!bodyMeasurementID) {
      throw new TypeError('bodyMeasurementID cannot be null');
    }
    try {
      const result = await API.del(
        'ampFamilog',
        `/bodymeasurements/${bodyMeasurementID}`,
        {
          headers: {},
          body: { bodyMeasurementID: bodyMeasurementID },
          response: true
        }
      );

      if (result) {
        const data = {
          action: 'DELETE_BODYMEASUREMENT',
          returnData: result.data
        };
        await commit('BODYMEASUREMENT_MUTATION', data);
      }
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  async updateBodyMeasurement({ commit }, payload) {
    const { bodyMeasurementID, bodyMeasurement } = payload;
    if (!bodyMeasurementID || !bodyMeasurement) {
      throw new TypeError(
        'bodyMeasurement or bodyMeasurementID cannot be null'
      );
    }
    try {
      const result = await API.put(
        'ampFamilog',
        `/bodymeasurements/${bodyMeasurementID}`,
        {
          response: true,
          body: bodyMeasurement
        }
      );
      if (result.data) {
        const data = {
          action: 'UPDATE_BODYMEASUREMENT',
          returnData: result.data
        };
        await commit('BODYMEASUREMENT_MUTATION', data);
      }
      return result.data;
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  /*
    Health Issue
  */
  async createHealthIssue({ commit }, payload) {
    const healthIssue = payload;

    if (!healthIssue) {
      throw new TypeError('healthIssue cannot be null');
    }
    try {
      const result = await API.post('ampFamilog', '/healthissues', {
        response: true,
        body: healthIssue
      });
      if (result.data) {
        const data = {
          action: 'ADD_HEALTHISSUE',
          returnData: result.data
        };
        await commit('HEALTHISSUE_MUTATION', data);
      }
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  async deleteHealthIssue({ commit }, payload) {
    const healthIssueID = payload;
    if (!healthIssueID) {
      throw new TypeError('healthIssueID cannot be null');
    }
    try {
      const result = await API.del(
        'ampFamilog',
        `/healthissues/${healthIssueID}`,
        {
          headers: {},
          body: { healthIssueID: healthIssueID },
          response: true
        }
      );

      if (result) {
        const data = {
          action: 'DELETE_HEALTHISSUE',
          returnData: result.data
        };
        await commit('HEALTHISSUE_MUTATION', data);
      }
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  },

  async updateHealthIssue({ commit }, payload) {
    const { healthIssueID, healthIssue } = payload;
    if (!healthIssueID || !healthIssue) {
      throw new TypeError('healthIssue or healthIssueID cannot be null');
    }
    try {
      const result = await API.put(
        'ampFamilog',
        `/healthissues/${healthIssueID}`,
        {
          response: true,
          body: healthIssue
        }
      );
      if (result.data) {
        const data = {
          action: 'UPDATE_HEALTHISSUE',
          returnData: result.data
        };
        await commit('HEALTHISSUE_MUTATION', data);
      }
      return result.data;
    } catch (error) {
      if (error.response.data && error.response.data.message) {
        throw new Error(error.response.data.message);
      }
      throw error;
    }
  }
};
