const state = {
  hospitalList: [],
  insuranceNoteList: [],
  healthTrainingList: [],
  examinationScheduleList: [],
  bodyMeasurementList: [],
  healthIssueList: []
};
export default state;
