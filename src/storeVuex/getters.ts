import { ACTIVE } from '@/constants/commonConst';
export default {
  getHospitalList: state => {
    return state.hospitalList.slice(0).filter(item => item.activity === ACTIVE);
  },
  getHospitalByID: state => {
    return function(id) {
      return state.hospitalList.find(item => item.hospitalID === id);
    };
  },
  getInsuranceNoteList: state => {
    return state.insuranceNoteList;
  },
  getInsuranceNoteByID: state => {
    return function(id) {
      return state.insuranceNoteList.find(item => item.insuranceNoteID === id);
    };
  },
  getHealthTrainingList: state => {
    return state.healthTrainingList;
  },
  getHealthTrainingByID: state => {
    return function(id) {
      return state.healthTrainingList.find(
        item => item.healthTrainingID === id
      );
    };
  },
  getExaminationScheduleList: state => {
    return state.examinationScheduleList;
  },
  getExaminationScheduleByID: state => {
    return function(id) {
      return state.examinationScheduleList.find(
        item => item.examinationScheduleID === id
      );
    };
  },
  getBodyMeasurementList: state => {
    return state.bodyMeasurementList;
  },
  getBodyMeasurementByID: state => {
    return function(id) {
      return state.bodyMeasurementList.find(
        item => item.bodyMeasurementID === id
      );
    };
  },
  getHealthIssueList: state => {
    return state.healthIssueList;
  },
  getHealthIssueByID: state => {
    return function(id) {
      return state.healthIssueList.find(item => item.healthIssueID === id);
    };
  }
};
