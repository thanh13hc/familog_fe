export default {
  GET_ALL_DATA(state, payload) {
    if (payload.bodyMeasurements) {
      state.bodyMeasurementList = [...payload.bodyMeasurements.data];
    } else {
      state.bodyMeasurementList = [];
    }
    if (payload.examinationSchedules) {
      state.examinationScheduleList = [...payload.examinationSchedules.data];
    } else {
      state.examinationScheduleList = [];
    }
    if (payload.healthIssues) {
      state.healthIssueList = [...payload.healthIssues.data];
    } else {
      state.healthIssueList = [];
    }
    if (payload.healthTrainings) {
      state.healthTrainingList = [...payload.healthTrainings.data];
    } else {
      state.healthTrainingList = [];
    }
    if (payload.hospitals) {
      state.hospitalList = [...payload.hospitals.data];
    } else {
      state.hospitalList = [];
    }
    if (payload.insuranceNotes) {
      state.insuranceNoteList = [...payload.insuranceNotes.data];
    } else {
      state.insuranceNoteList = [];
    }
  },

  HOSPITAL_MUTATION(state, payload) {
    const { action, returnData } = payload;
    switch (action) {
      case 'ADD_HOSPITAL':
        state.hospitalList = [...state.hospitalList, returnData];
        return;

      case 'DELETE_HOSPITAL':
        state.hospitalList = state.hospitalList.filter(
          hospitals => hospitals.hospitalID !== returnData.hospitalID
        );
        return;

      case 'UPDATE_HOSPITAL':
        state.hospitalList = state.hospitalList.map(hospital =>
          hospital.hospitalID === returnData.hospitalID ? returnData : hospital
        );
        return;
    }
  },

  INSURANCENOTE_MUTATION(state, payload) {
    const { action, returnData } = payload;
    switch (action) {
      case 'ADD_INSURANCENOTE':
        state.insuranceNoteList = [...state.insuranceNoteList, returnData];
        return;

      case 'DELETE_INSURANCENOTE':
        state.insuranceNoteList = state.insuranceNoteList.filter(
          insuranceNote =>
            insuranceNote.insuranceNoteID !== returnData.insuranceNoteID
        );
        return;

      case 'UPDATE_INSURANCENOTE':
        state.insuranceNoteList = state.insuranceNoteList.map(insuranceNote =>
          insuranceNote.insuranceNoteID === returnData.insuranceNoteID
            ? returnData
            : insuranceNote
        );

        return;
    }
  },

  HEALTHTRAINING_MUTATION(state, payload) {
    const { action, returnData } = payload;
    switch (action) {
      case 'ADD_HEALTHTRAINING':
        state.healthTrainingList = [...state.healthTrainingList, returnData];
        return;

      case 'DELETE_HEALTHTRAINING':
        state.healthTrainingList = state.healthTrainingList.filter(
          healthTraining =>
            healthTraining.healthTrainingID !== returnData.healthTrainingID
        );
        return;

      case 'UPDATE_HEALTHTRAINING':
        state.healthTrainingList = state.healthTrainingList.map(
          healthTraining =>
            healthTraining.healthTrainingID === returnData.healthTrainingID
              ? returnData
              : healthTraining
        );
        return;
    }
  },

  EXAMINATIONSCHEDULE_MUTATION(state, payload) {
    const { action, returnData } = payload;
    switch (action) {
      case 'ADD_EXAMINATIONSCHEDULE':
        state.examinationScheduleList = [
          ...state.examinationScheduleList,
          returnData
        ];
        return;

      case 'DELETE_EXAMINATIONSCHEDULE':
        state.examinationScheduleList = state.examinationScheduleList.filter(
          examinationSchedule =>
            examinationSchedule.examinationScheduleID !==
            returnData.examinationScheduleID
        );
        return;

      case 'UPDATE_EXAMINATIONSCHEDULE':
        state.examinationScheduleList = state.examinationScheduleList.map(
          examinationSchedule =>
            examinationSchedule.examinationScheduleID ==
            returnData.examinationScheduleID
              ? returnData
              : examinationSchedule
        );
        return;
    }
  },

  BODYMEASUREMENT_MUTATION(state, payload) {
    const { action, returnData } = payload;
    switch (action) {
      case 'ADD_BODYMEASUREMENT':
        state.bodyMeasurementList = [...state.bodyMeasurementList, returnData];
        return;

      case 'DELETE_BODYMEASUREMENT':
        state.bodyMeasurementList = state.bodyMeasurementList.filter(
          bodyMeasurement =>
            bodyMeasurement.bodyMeasurementID !== returnData.bodyMeasurementID
        );
        return;

      case 'UPDATE_BODYMEASUREMENT':
        state.bodyMeasurementList = state.bodyMeasurementList.map(
          bodyMeasurement =>
            bodyMeasurement.bodyMeasurementID == returnData.bodyMeasurementID
              ? returnData
              : bodyMeasurement
        );
        return;
    }
  },

  HEALTHISSUE_MUTATION(state, payload) {
    const { action, returnData } = payload;
    switch (action) {
      case 'ADD_HEALTHISSUE':
        state.healthIssueList = [...state.healthIssueList, returnData];
        return;

      case 'DELETE_HEALTHISSUE':
        state.healthIssueList = state.healthIssueList.filter(
          healthIssue => healthIssue.healthIssueID !== returnData.healthIssueID
        );
        return;

      case 'UPDATE_HEALTHISSUE':
        state.bodyMeasurementList = state.bodyMeasurementList.map(healthIssue =>
          healthIssue.healthIssueID == returnData.healthIssueID
            ? returnData
            : healthIssue
        );
        return;
    }
  }
};
