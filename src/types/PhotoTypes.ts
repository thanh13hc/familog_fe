export type PhotoType = {
  s3: S3Type | null;
  fileName: string;
  base64Image?: string | null;
};

export type S3Type = {
  Location: string;
  ETag: string;
  Bucket: string;
  Key: string;
};
