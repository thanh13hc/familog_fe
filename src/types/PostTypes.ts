import { PhotoType } from './PhotoTypes';
import { UserType } from './UserTypes';

export type PostType = {
  postID?: string;
  category: string;
  subCategory?: string | null;
  title: string;
  text: string;
  detail?: { [key: string]: object } | null;
  userID: string;
  user: UserType;
  sharedID?: string[] | null;
  photos?: PhotoType[] | null;
  createdAt?: number;
  updatedAt?: number;
  [key: string]: any;
};
