import { PhotoType } from './PhotoTypes';

export type UserType = {
  userID?: string;
  surname: string;
  name: string;
  birthday: number;
  countryCode: string;
  phoneNumber: string;
  profile: PhotoType | null;
  cover: PhotoType | null;
  profileID: string;
  endingID: string | null;
  propertyID: string | null;
  friendID: string[] | null;
  medicalUpdatedAt?: number | null;
  propertyUpdatedAt?: number | null;
  educationUpdatedAt?: number | null;
  memoryUpdatedAt?: number | null;
  communicationUpdatedAt?: number | null;
  jobUpdatedAt?: number | null;
  configUpdatedAt?: number | null;
  purchaseUpdatedAt?: number | null;
  inheritanceUpdatedAt?: number | null;
  signInedAt?: number | null;
  createdAt?: number;
  updatedAt?: number;
  [key: string]: string | number | null | undefined | string[] | PhotoType;
};
