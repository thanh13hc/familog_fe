import { UserType } from '@/types';

interface StoreInterface {
  state: {
    loginUser: UserType;
    showID: string;
    friendUser: UserType;
  };
  setLoginUser: (userData: UserType) => void;
  addFriend: (friendID: string) => void;
  setShowID: (userID: string) => void;
  setFriendUser: (userData: UserType) => void;
}

export const store: StoreInterface = {
  state: {
    loginUser: {
      userID: '',
      surname: '',
      name: '',
      birthday: 0,
      countryCode: '',
      phoneNumber: '',
      profileID: null,
      endingID: null,
      propertyID: null,
      profile: null,
      cover: null,
      friendID: null,
      signInedAt: null,
      createdAt: undefined,
      updatedAt: undefined
    },
    showID: '',
    friendUser: {
      userID: '',
      surname: '',
      name: '',
      birthday: 0,
      countryCode: '',
      phoneNumber: '',
      profileID: null,
      endingID: null,
      propertyID: null,
      profile: null,
      cover: null,
      friendID: [],
      signInedAt: null,
      createdAt: undefined,
      updatedAt: undefined
    }
  },
  setLoginUser(userData: UserType) {
    this.state.loginUser.userID = userData.userID;
    this.state.loginUser.surname = userData.surname;
    this.state.loginUser.name = userData.name;
    this.state.loginUser.birthday = userData.birthday;
    this.state.loginUser.countryCode = userData.countryCode;
    this.state.loginUser.phoneNumber = userData.phoneNumber;
    this.state.loginUser.profileID = userData.profileID ?? null;
    this.state.loginUser.endingID = userData.endingID ?? null;
    this.state.loginUser.propertyID = userData.propertyID ?? null;
    this.state.loginUser.profile = userData.profile;
    this.state.loginUser.cover = userData.cover;
    this.state.loginUser.signInedAt = userData.signInedAt;
    this.state.loginUser.friendID = userData.friendID;
  },
  addFriend(friendID: string) {
    if (!Array.isArray(this.state.loginUser.friendID)) {
      this.state.loginUser.friendID = [friendID];
    } else {
      this.state.loginUser.friendID.push(friendID);
    }
  },
  setShowID(userID: string) {
    this.state.showID = userID;
  },
  setFriendUser(userData: UserType) {
    this.state.friendUser.userID = userData.userID;
    this.state.friendUser.surname = userData.surname;
    this.state.friendUser.name = userData.name;
    this.state.friendUser.birthday = userData.birthday;
    this.state.friendUser.countryCode = userData.countryCode;
    this.state.friendUser.phoneNumber = userData.phoneNumber;
    this.state.friendUser.profileID = userData.profileID ?? null;
    this.state.friendUser.endingID = userData.endingID ?? null;
    this.state.friendUser.propertyID = userData.propertyID ?? null;
    this.state.friendUser.profile = userData.profile;
    this.state.friendUser.cover = userData.cover;
    this.state.friendUser.signInedAt = userData.signInedAt;
    this.state.friendUser.friendID = userData.friendID;
  }
};
