export { default as CoverPhoto } from './CoverPhoto.vue';
export { default as ProfilePhoto } from './ProfilePhoto.vue';
export { default as RoundPhoto } from './RoundPhoto.vue';
export { default as RoundPhotoExtraSmall } from './RoundPhotoExtraSmall.vue';
export { default as RoundPhotoSmall } from './RoundPhotoSmall.vue';
