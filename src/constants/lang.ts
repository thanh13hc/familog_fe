export const SIGN_UP_TITLE = '新規登録';
export const DELETE_TITLE = '削除';
export const EDIT_TITLE = '編集';
export const SAVE_TITLE = '保存';
export const REGISTRATION = '登録';
export const SELECT_PLACEHOLDER = '選択してください';
export const INPUT_TEXT_PLACEHOLDER = '入力してください';
export const YEAR = '年';
export const MONTH = '月';
export const DAY = '日';
export const DATE_FORMAT = 'YYYY年MM月DD日';
export const DATE_FORMAT_DISPLAY = 'YYYY.MM.DD';

export const SELECTED = '選択済み';
export const UNSELECTED = '未選択';
export const TAP_TO_SELECT_MESSAGE = 'タップして選択';

export const ERROR_REQUIRED = 'この項目は必須です';
export const ERROR_NUMBER_FORMAT = '入力した番号フォーマットは無効です';
export const CHECK_DATE_ERROR = '選択した日付は無効です';
export const ERROR_PHONE_NUMBER = '電話番号のフォーマットが無効です';
export const NOT_FOUND_POSTALCODE = '郵便番号は検出できません';

export const TITLE = 'タイトル';
export const DATE = '日付';
export const TIME = '時間';
export const HOSPITAL = '病院';
export const REGISTER_A_HOSPITAL = '病院を登録する';
export const TYPE = '種別';
export const CONTENT = '内容';

/*
  EXAMINATION SCHEDULE
*/
// FA_04.1.1,FA_04.1.2:
export const MEDICAL_EXAMINATION_REGISTER_TITLE = '特定検診の記録';
export const MEDICAL_EXAMINATION_DETAIL_TITLE = '検診の詳細';
export const MEDICAL_EXAMINATION_EDIT_TITLE = '特定検診の記録';
export const EXAMINATION_CONTENT = '検診内容';

// FA_04.1: Medical examination calendar
export const MEDICAL_EXAMINATION_CALENDAR_TITLE = '検診カレンダー';
export const UNREGISTERED_EXAMINATION_CALENDAR_MESSAGE =
  'まだ健康診断は登録されていません';
export const TODAY = '今日';
export const DAY_AFTER = '日後'; // ex: 7日後, 13日後

export const REGISTRATION_EXAMINATION_SCHEDULE_ERR_MESSAGE =
  '検診カレンダー登録は失敗しました';
export const EDIT_EXAMINATION_SCHEDULE_ERR_MESSAGE =
  '検診カレンダー編集は失敗しました';
export const DELETE_EXAMINATION_SCHEDULE_ERR_MESSAGE =
  '検診カレンダー削除は失敗しました';
export const CONFIRM_EXAMINATION_SCHEDULE_DELETION =
  '検診カレンダーは本当に削除しますか？';

/*
  PRIMARY CARE HOSPITAL
*/
// FA_04.2_b, FA_04.2_b. Primary care hospital
export const PRIMARY_CARE_HOSPITAL_TITLE = 'かかりつけの病院';
export const UNREGISTERED_HOSPITAL_MESSAGE = '病院が登録されていません';
export const CALL = '電話する';

// FA_04.2.1, FA_04.2.2, FA_04.2.3 Primary care hospital regis, edit, detail
export const PRIMARY_CARE_HOSPITAL_HOSPITAL_REGISTER_TITLE = '病院を登録する';
export const PRIMARY_CARE_HOSPITAL_DEPARTMENT_REGISTER_TITLE =
  '診療科を登録する';
export const PRIMARY_CARE_HOSPITAL_HOSPITAL_EDIT_TITLE = '病院を編集する';
export const PRIMARY_CARE_HOSPITAL_DEPARTMENT_EDIT_TITLE = '診療科を編集する';
export const PRIMARY_CARE_HOSPITAL_DETAIL_TITLE = '病院の詳細';

export const HOSPITAL_NAME = '病院名';
export const POSTAL_CODE = '郵便番号';
export const PREFECTURES = '都道府県';
export const ADDRESS = '住所';
export const AUTO_ENTER_ADDRESS = '住所を自動入力';
export const CLOSED_DAYS = '休診日';
export const PHONE_NUMBER = '電話番号';
export const EMERGENCY_WARD = '救急病棟';
export const DEPARTMENT = '診療科';
export const DOCTOR = '担当医の名前（任意）';
export const ADD_ROW = '行を追加する';

export const REGISTRATION_HOSPITAL_ERR_MESSAGE = '病院登録に失敗しました';
export const EDIT_HOSPITAL_ERR_MESSAGE = '病院編集は失敗しました';
export const DELETE_HOSPITAL_ERR_MESSAGE = '病院削除は失敗しました';
export const CONFIRM_HOSPITAL_DELETION = 'この病院は本当に削除しますか？';

/*
  BODY_MEASUREMENT
*/
// FA_04.3.  body measurement
export const BODY_MEASUREMENT_TITLE = '特定検診の記録';
export const JUDGMENT = '判定';
export const EXECUTING_AGENCY = '実施機関';
export const UNREGISTERED_BODY_MEASUREMENT_MESSAGE =
  '記録はまだ登録されていません';

// FA_04.3.1. medical examination record- Register of medical examination record
export const BODY_MEASUREMENT_REGISTER_TITLE = '検診記録を登録する';
export const BODY_MEASUREMENT_EDIT_TITLE = '試験記録を編集する';
export const METALBOLIC_SYNDROME_JUDGMENT = 'メタボリックシンドローム判定';

export const REGISTRATION_BODY_MEASUREMENT_ERR_MESSAGE =
  '記録登録に失敗しました';
export const EDIT_BODY_MEASUREMENT_ERR_MESSAGE = '記録編集は失敗しました';
export const DELETE_BODY_MEASUREMENT_ERR_MESSAGE = '記録削除は失敗しました';
export const CONFIRM_BODY_MEASUREMENT_DELETION =
  'この記録は本当に削除しますか？';

/*
  HEALTH TRAINING
*/
// FA_04.4. Health training
export const HEALTH_TRAINING_TITLE = '健康教育';
export const HEALTH_TRAINING_REGISTER_TITLE = '健康教育を登録する';
export const HEALTH_TRAINING_EDIT_TITLE = '健康教育のために編集する';
export const HEALTH_TRAINING_DETAIL_TITLE = '健康教育の詳細';

export const UNREGISTERED_HEALTH_TRAINING_MESSAGE =
  '記録はまだ登録されていません';
export const REGISTRATION_HEALTH_TRAINING_ERR_MESSAGE =
  '健康教育登録に失敗しました';
export const EDIT_HEALTH_TRAINING_ERR_MESSAGE = '健康教育編集は失敗しました';
export const DELETE_HEALTH_TRAINING_ERR_MESSAGE = '健康教育削除は失敗しました';
export const CONFIRM_HEALTH_TRAINING_DELETION =
  '健康教育は本当に削除しますか？';

/*
  HEALTH ISSUE
*/
// FA_04.5. Health Issue
export const HEALTH_ISSUE_TITLE = '各検診の記録';
export const HEALTH_ISSUE_RECORD = '胃がん検診';
export const HEALTH_ISSUE_RECORD_REGISTER_TITLE = '検診記録の登録';
export const HEALTH_ISSUE_RECORD_EDIT_TITLE = '検査記録の編集';
export const HEALTH_ISSUE_RECORD_DETAIL_TITLE = '検診記録の詳細';

// FA_04.5.1a.x. Each examination record –Details of medical examination record
export const DATE_OF_CONSULTATION = '受診年月日';
export const EXECUTING_AGENCY_NAME = '実施機関名';
export const EXTRA_DATE_OF_CONSULTATION = '受診予定日';
export const EXTRA_EXECUTING_AGENCY_NAME = '実施機関名';
export const SPUTUM_CYTOLOGY = '喀痰細胞診';
export const SCHEDULE_DETAIL_INSPECTION = '精密検査の予定';
export const DETAILED_EXAMINATION = '精密検査';

export const REGISTRATION_HEALTH_ISSUE_ERR_MESSAGE =
  '検診記録の登録に失敗しました';
export const UNREGISTERED_HEALTH_ISSUE_MESSAGE = '記録はまだ登録されていません';
export const EDIT_HEALTH_ISSUE_ERR_MESSAGE = '検診記録編集に失敗しました';
export const DELETE_HEALTH_ISSUE_ERR_MESSAGE = '検診記録削除は失敗しました';
export const CONFIRM_HEALTH_ISSUE_DELETION = '検診記録は本当に削除しますか？';

/*
  INSURANCE NOTE
*/
// FA_04.6.  Insurance Note
export const INSURANCE_NOTE_TITLE = '特定保険指導';
export const INSURANCE_NOTE_REGISTER_TITLE = '特定保険指導を登録する';
export const INSURANCE_NOTE_EDIT_TITLE = '特定の保険ガイダンスを編集する';
export const INSURANCE_NOTE_DETAIL_TITLE = '特定保険指導の詳細';

export const UNREGISTERED_INSURANCE_NOTE_MESSAGE =
  '記録はまだ登録されていません';
export const REGISTRATION_INSURANCE_NOTE_ERR_MESSAGE = '保険登録に失敗しました';
export const EDIT_INSURANCE_NOTE_ERR_MESSAGE = '保険編集に失敗しました';
export const DELETE_INSURANCE_NOTE_ERR_MESSAGE = '保険削除に失敗しました';
export const CONFIRM_INSURANCE_NOTE_DELETION = '保険は本当に削除しますか？';
