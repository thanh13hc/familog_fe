export const POST_CATEGORY: {
  [key: string]: { [key: string]: string };
} = Object.freeze({
  children: {
    id: 'children',
    text: '親子手帳',
    value: 'children',
    detail: '我が子の発育の記録'
  },
  learnHistory: {
    id: 'learnHistory',
    text: '学歴手帳',
    value: 'learnHistory',
    detail: '生涯を通した学びの記録'
  },
  budget: {
    id: 'budget',
    text: '家計手帳',
    value: 'budget',
    detail: '家族にとって価値ある買い物の記録'
  },
  health: {
    id: 'health',
    text: '健康手帳',
    value: 'health',
    detail: '病歴、通院歴、投薬の記録',
    componentName: 'HealthNotebook'
  },
  career: {
    id: 'career',
    text: '職歴手帳',
    value: 'career',
    detail: '積み上げたキャリアの記録'
  },
  property: {
    id: 'property',
    text: '財産手帳',
    value: 'property',
    detail: '家族のための蓄財の記録'
  },
  memory: {
    id: 'memory',
    text: '記念手帳',
    value: 'memory',
    detail: '思い出の写真や動画の記録'
  },
  relation: {
    id: 'relation',
    text: '間柄手帳',
    value: 'relation',
    detail: '縁ある人と家系の記録'
  },
  endOfLifePlanning: {
    id: 'endOfLifePlanning',
    text: '終活手帳',
    value: 'endOfLifePlanning',
    detail: '自分らしい人生の記録',
    url: 'end-of-life',
    componentName: 'EndingCategory'
  }
});

const DEFAULT_ENDING_DATA = Object.freeze({
  title: {
    name: 'タイトル',
    type: 'string'
  },
  description: {
    name: '自由記述欄',
    type: 'string'
  },
  notifyWay: {
    name: '送付方法',
    type: 'string'
  },
  notifyTiming: {
    name: '送付タイミング',
    type: 'string'
  }
});

export const ENDING_CATEGORY = Object.freeze({
  ending: {
    name: 'もしものとき',
    subCategory: {
      insurance: {
        name: '保険の受け取り方',
        data: DEFAULT_ENDING_DATA
      },
      will: {
        name: '遺言',
        data: DEFAULT_ENDING_DATA
      },
      successor: {
        name: '後継人',
        data: DEFAULT_ENDING_DATA
      },
      cure: {
        name: '延命治療',
        data: DEFAULT_ENDING_DATA
      },
      care: {
        name: '介護',
        data: DEFAULT_ENDING_DATA
      },
      funeral: {
        name: '葬儀',
        data: DEFAULT_ENDING_DATA
      },
      burial: {
        name: '埋葬',
        data: DEFAULT_ENDING_DATA
      },
      pet: {
        name: 'ペットの扱い',
        data: DEFAULT_ENDING_DATA
      }
    }
  },
  property: {
    name: '資産',
    subCategory: {
      bank: {
        name: '銀行',
        data: {
          bankName: {
            name: '銀行名',
            type: 'string'
          },
          branchName: {
            name: '支店名',
            type: 'string'
          },
          accountCategory: {
            name: '口座種別',
            type: 'string'
          },
          ownerName: {
            name: '名義人名',
            type: 'string'
          },
          accountNumber: {
            name: '口座番号',
            type: 'string'
          },
          code: {
            name: '暗証番号',
            type: 'string'
          },
          amount: {
            name: '残高',
            type: 'string'
          },
          regularWithdraw: {
            name: '定期引落先',
            type: 'string'
          },
          bankMan: {
            name: '銀行担当者',
            type: 'string'
          },
          bankManContact: {
            name: '銀行担当者連絡先',
            type: 'string'
          },
          placeOfLedger: {
            name: '通帳の在り処',
            type: 'string'
          },
          placeOfStamp: {
            name: 'お届け印の在り処',
            type: 'string'
          },
          ibID: {
            name: 'IBのID',
            type: 'string'
          },
          ibPassword: {
            name: 'IBのパスワード（１次認証）',
            type: 'string'
          },
          ibPassword2: {
            name: 'IBのパスワード（２次認証）',
            type: 'string'
          }
        }
      },
      insurance: {
        name: '保険',
        data: {
          companyName: {
            name: '保険会社',
            type: 'string'
          },
          branchName: {
            name: '支店名',
            type: 'string'
          },
          category: {
            name: '保険種別',
            type: 'string'
          },
          documentNumber: {
            name: '保険証書番号',
            type: 'string'
          },
          placeOfStamp: {
            name: 'お届け印の在り処',
            type: 'string'
          },
          insuranceMan: {
            name: '保険会社担当者（連絡先）',
            type: 'string'
          },
          placeOfDocument: {
            name: '保険証書の在り処',
            type: 'string'
          },
          ID: {
            name: 'ID',
            type: 'string'
          },
          password: {
            name: 'パスワード',
            type: 'string'
          }
        }
      },
      stock: {
        name: '株式',
        data: {
          companyName: {
            name: '取引証券会社',
            type: 'string'
          },
          branchName: {
            name: '支店名',
            type: 'string'
          },
          companyMan: {
            name: '担当者（連絡先）',
            type: 'string'
          },
          issuer: {
            name: '銘柄',
            type: 'string'
          },
          number: {
            name: '枚数',
            type: 'string'
          },
          assessedValue: {
            name: '評価額',
            type: 'string'
          },
          ID: {
            name: 'ID',
            type: 'string'
          },
          password: {
            name: 'パスワード',
            type: 'string'
          }
        }
      },
      movableProperty: {
        name: '動産',
        data: {
          propertyName: {
            name: '物品名',
            type: 'string'
          },
          place: {
            name: '在り処',
            type: 'string'
          },
          buyFrom: {
            name: '取引先（購入先）',
            type: 'string'
          },
          contactPaper: {
            name: '契約書等',
            type: 'string'
          }
        }
      },
      realEstate: {
        name: '不動産',
        data: {
          postalCode: {
            name: '郵便番号',
            type: 'string'
          },
          address1: {
            name: '住所',
            type: 'string'
          },
          address2: {
            name: '地番',
            type: 'string'
          },
          purpose: {
            name: '用途',
            type: 'string'
          },
          owner: {
            name: '所有者',
            type: 'string'
          },
          ownerAddress: {
            name: '所有者住所',
            type: 'string'
          },
          ownerPhoneNumber: {
            name: '所有者電話番号',
            type: 'string'
          },
          ownerRatio: {
            name: '所有者割合',
            type: 'string'
          },
          managingCompany: {
            name: '管理会社',
            type: 'string'
          },
          companyMan: {
            name: '担当者（連絡先）',
            type: 'string'
          },
          placeOfContract: {
            name: '契約書の在り処',
            type: 'string'
          },
          placeOfDocument: {
            name: '登記簿謄本の在り処',
            type: 'string'
          },
          placeOfStamp: {
            name: '契約印の在り処',
            type: 'string'
          },
          assessedValue: {
            name: '評価額',
            type: 'string'
          }
        }
      },
      debt: {
        name: '負債',
        data: {
          companyName: {
            name: 'ローン債権会社名',
            type: 'string'
          },
          branchName: {
            name: '支店名',
            type: 'string'
          },
          companyMan: {
            name: '銀行担当者',
            type: 'string'
          },
          contract: {
            name: 'ローン契約内容',
            type: 'string'
          },
          amount: {
            name: '負債額',
            type: 'string'
          },
          repayments: {
            name: '返済額',
            type: 'string'
          },
          placeOfContract: {
            name: '契約書の在り処',
            type: 'string'
          },
          placeOfStamp: {
            name: '契約印の在り処',
            type: 'string'
          },
          repaymentDeadline: {
            name: '約定返済日',
            type: 'string'
          }
        }
      }
    }
  },
  profile: {
    name: '自分のこと',
    subCategory: {
      attribute: {
        name: '属性情報',
        data: {
          surname: {
            name: '苗字',
            type: 'string'
          },
          name: {
            name: '名前',
            type: 'string'
          },
          surnameKana: {
            name: '氏名かな',
            type: 'string'
          },
          nameKana: {
            name: '氏名かな',
            type: 'string'
          },
          birthday: {
            name: '生年月日',
            type: 'date'
          },
          gender: {
            name: '性別',
            type: 'string'
          },
          domicile: {
            name: '本籍地履歴',
            type: 'string'
          },
          phoneNumber: {
            name: '電話番号（携帯電話）',
            type: 'string'
          },
          homePhoneNumber: {
            name: '電話番号（自宅電話）',
            type: 'string'
          },
          bloodType: {
            name: '血液型',
            type: 'string'
          }
        }
      },
      document: {
        name: '身分証明書',
        data: {
          myNumber: {
            name: 'マイナンバー',
            type: 'string'
          },
          placeOfMyNumber: {
            name: 'マイナンバーカード保存場所',
            type: 'string'
          },
          driverLicenceNumber: {
            name: '運転免許証番号',
            type: 'string'
          },
          placeOfDriverLicenceNumber: {
            name: '運転免許証保存場所',
            type: 'string'
          },
          nationalInsuranceNumber: {
            name: '保険証番号',
            type: 'string'
          },
          placeOfNationalInsurance: {
            name: '保険証保存場所',
            type: 'string'
          },
          passportNumber: {
            name: '保険証番号',
            type: 'string'
          },
          placeOfPassport: {
            name: '保険証保存場所',
            type: 'string'
          },
          placeOfStamp: {
            name: '実印の保管場所',
            type: 'string'
          }
        }
      },
      history: {
        name: '自分史',
        data: {
          namedAfter: {
            name: '名前の由来',
            type: 'string'
          },
          education: {
            name: '学歴',
            type: 'string'
          },
          job: {
            name: '職歴',
            type: 'string'
          },
          episode: {
            name: 'エピソード',
            type: 'string'
          }
        }
      }
    }
  }
});
