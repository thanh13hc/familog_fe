export const EMERGENCY_WARD_LIST: readonly {
  [key: string]: string;
}[] = Object.freeze([
  { text: '内科', value: 'InternalMedicine' },
  { text: '小児科', value: 'Pediatrics' },
  { text: '皮膚科', value: 'Dermatology' },
  { text: '婦人科', value: 'Gynecology' },
  { text: '産科', value: 'Obstetrics' },
  { text: '産婦人科', value: 'ObstetricsAndGynecology' },
  { text: '耳鼻いんこう科', value: 'Otolaryngology' },
  { text: '眼科', value: 'Ophthalmology' },
  { text: '整形外科', value: 'Orthopedics' },
  { text: 'アレルギー科', value: 'AllergyDepartment' },
  { text: '泌尿器科', value: 'Urology' },
  { text: '肛門外科', value: 'AnalSurgery' },
  { text: '胃腸内科', value: 'Gastroenterologys' },
  { text: '気管食道内科', value: 'TracheoesophagealInternalMedicine' },
  { text: '胸部外科', value: 'ThoracicSurgery' },
  { text: '形成外科', value: 'PlasticSurgery' },
  { text: '血液内科', value: 'Hematology' },
  { text: '外科', value: 'Surgery' },
  { text: '呼吸器内科', value: 'RespiratoryMedicine' },
  { text: '呼吸器外科', value: 'RespiratorySurgery' },
  { text: '心療内科', value: 'PsychosomaticMedicine' },
  { text: '消化器内科', value: 'Gastroenterology' },
  { text: '脳神経内科', value: 'Neurology' },
  { text: '心臓血管外科', value: 'CardiovascularSurgery' },
  { text: '消化器外科', value: 'DigestiveSurgery' },
  { text: '小児外科', value: 'PediatricSurgery' },
  { text: '循環器内科', value: 'Cardiology' },
  { text: '腎臓内科', value: 'Nephrology' },
  { text: '精神科', value: 'Psychiatry' },
  { text: '糖尿病内科', value: 'DiabetesInternalMedicine' },
  { text: '内分泌内科', value: 'Endocrinology' },
  { text: '乳腺外科', value: 'BreastSurgery' },
  { text: '脳神経外科', value: 'Neurosurgery' },
  { text: '美容外科', value: 'CosmeticSurgery' },
  { text: 'ペインクリニック内科', value: 'PainClinicInternalMedicine' },
  { text: '放射線科', value: 'Radiology' },
  { text: '麻酔科', value: 'Anesthesiology' },
  { text: 'リハビリテーション科', value: 'DepartmentOfRehabilitation' },
  { text: 'リウマチ科', value: 'Rheumatism' },
  { text: '老年内科', value: 'Geriatrics' },
  { text: '歯科', value: 'Dentistry' }
]);

export const JAPAN_PROVINCE: readonly {
  [key: string]: string;
}[] = Object.freeze([
  { text: '北海道', value: 'Hokkaido' },
  { text: '青森県', value: 'Aomori' },
  { text: '岩手県', value: 'Iwate' },
  { text: '宮城県', value: 'Miyagi' },
  { text: '秋田県', value: 'Akita' },
  { text: '山形県	', value: 'Yamagata' },
  { text: '福島県', value: 'Fukushima' },
  { text: '茨城県', value: 'Ibaraki' },
  { text: '栃木県', value: 'Tochigi' },
  { text: '群馬県', value: 'Gunma' },
  { text: '埼玉県', value: 'Saitama' },
  { text: '千葉県', value: 'Chiba' },
  { text: '東京都', value: 'Tokyo' },
  { text: '神奈川県', value: 'Kanagawa' },
  { text: '新潟県', value: 'Niigata' },
  { text: '富山県', value: 'Toyama' },
  { text: '石川県', value: 'Ishikawa' },
  { text: '福井県', value: 'Fukui' },
  { text: '山梨県', value: 'Yamanashi' },
  { text: '長野県', value: 'Nagano' },
  { text: '岐阜県', value: 'Gifu' },
  { text: '静岡県', value: 'Shizuoka' },
  { text: '愛知県', value: 'Aichi' },
  { text: '三重県', value: 'Mie' },
  { text: '滋賀県', value: 'Shiga' },
  { text: '京都府', value: 'Kyoto' },
  { text: '大阪府', value: 'Osaka' },
  { text: '兵庫県', value: 'Hyōgo' },
  { text: '奈良県', value: 'Nara' },
  { text: '和歌山県', value: 'Wakayama' },
  { text: '鳥取県', value: 'Tottori' },
  { text: '島根県', value: 'Shimane' },
  { text: '岡山県', value: 'Okayama' },
  { text: '広島県', value: 'Hiroshima' },
  { text: '山口県', value: 'Yamaguchi' },
  { text: '徳島県', value: 'Tokushima' },
  { text: '香川県', value: 'Kagawa' },
  { text: '愛媛県', value: 'Ehime' },
  { text: '高知県', value: 'Kōchi' },
  { text: '福岡県', value: 'Fukuoka' },
  { text: '佐賀県', value: 'Saga' },
  { text: '長崎県', value: 'Nagasaki' },
  { text: '熊本県	', value: 'Kumamoto' },
  { text: '大分県', value: 'Ōita' },
  { text: '宮崎県', value: 'Miyazaki' },
  { text: '鹿児島県', value: 'Kagoshima' },
  { text: '沖縄県', value: 'Okinawa' }
]);

export const HEALTH_TRAINING_OPTIONS_TYPE: readonly {
  [key: string]: string;
}[] = Object.freeze([
  {
    value: 'healthEducation',
    text: '健康教育'
  },
  {
    value: 'healthConsultation',
    text: '健康相談'
  },
  {
    value: 'visitGuidance',
    text: '訪問指導'
  }
]);

export const INSURANCE_NOTE_OPTIONS_TYPE: readonly {
  [key: string]: string;
}[] = Object.freeze([
  {
    value: 'activelySupport',
    text: '積極的支援'
  },
  {
    value: 'motivationSupport',
    text: '動機付け支援'
  },
  {
    value: 'none',
    text: 'なし'
  }
]);
