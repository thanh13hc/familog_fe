export const YES = 1;
export const NO = 0;

export const YEAR_RANGE = 5;

export const ACTIVE = 0;
export const DEACTIVE = 1;

export const DISPLAY_LIST = 'list';
export const DISPLAY_CALENDAR = 'calendar';

export const MULTIPLE_SELECT = 'multiple';

export const DEFAULT_MUTED_BOX = 'default';
export const DESCRIPTION_MUTED_BOX = 'description';

export const REGISTERED = 'registered';
export const NOT_REGISTERED = 'not registered';
export const MAX_TEXT_AREA_LENGTH = 200;
export const MAX_TEXT_AREA_DISPLAY = 50;
export const MAX_TEXT_LENGTH = 30;

export const STATUS_FIELD = 'status';
export const ECG = '8';
export const FUNDUS_EXAMINATION = '9';

export const WEEKLIST = [
  { value: 1, text: '月曜日' },
  { value: 2, text: '火曜日' },
  { value: 3, text: '水曜日' },
  { value: 4, text: '木曜日' },
  { value: 5, text: '金曜日' },
  { value: 6, text: '土曜日' },
  { value: 0, text: '日曜日' }
];

export const SUNDAY = 0;

export const RegisFail = '登録に失敗しました';

export const EditFail = '失敗した変更';

export const displayMore = [
  {
    value: true,
    text: '要精検'
  },
  { value: false, text: '異常なし' }
];

export const sputumCytology = [
  {
    value: 'implementation',
    text: '実施'
  },
  {
    value: 'notImplemented',
    text: '未実施'
  }
];

export const BODY_MEASUREMENT_FIELD = [
  [
    'dateOfConsultation',
    'height',
    'bodyWeight',
    'waistCircumference',
    'BMI',
    'bloodPressure'
  ],
  [
    'TP',
    'albumin',
    'UA',
    'WBC',
    'PLT',
    'CRP',
    'BUN',
    'NA',
    'K',
    'CI',
    'T_BIL',
    'D_BIL',
    'LDH',
    'ALP',
    'CH_E',
    'CPK',
    'AMY',
    'PT',
    'APTT',
    'D',
    'ferritin'
  ],
  ['neutralFat', 'HDLCholesterol', 'LDLCholesterol'],
  ['AST', 'ALT', 'y_GT'],
  ['fastingGlucose', 'hemoglobinA1c'],
  ['sugar', 'white'],
  ['numberOfRedBloodCells', 'hemoglobin', 'hematocrit'],
  ['serumCreatinine', 'eGFR'],
  ['ECGStatus'],
  ['fundusExaminationStatus'],
  [
    'medicalHistory',
    'medicationHistory',
    'smokingHistory',
    'subjectiveSymptoms',
    'objectiveSymptoms'
  ]
];
export const STATUS_OPTIONS = [
  { text: '異常認めず', value: 'noAbnormalityFound' },
  { text: '異常あり (疑)', value: 'abnormal' }
];

export const MEDICALEXAM = [
  {
    id: '0',
    value: 'Medical examination medical inquiry physical measurement',
    nihonName: '検診等、問診、身体計測',
    field: [
      {
        name: '受診年月日'
      },
      {
        name: '身長'
      },
      {
        name: '体重'
      },
      {
        name: '腹囲'
      },
      {
        name: 'BMI'
      },
      {
        name: '血圧（収縮期〜拡張期）/mmgh'
      }
    ],
    data: REGISTERED
  },
  {
    id: '1',
    value: 'Blood test',
    nihonName: '血液検査',
    field: [
      {
        name: 'TP（総蛋白）'
      },
      {
        name: 'アルブミン'
      },
      {
        name: 'UA（尿酸）'
      },
      {
        name: 'WBC（白血球）'
      },
      {
        name: 'PLT（血小板）'
      },
      {
        name: 'CRP'
      },
      {
        name: 'BUN（尿素窒素）'
      },
      {
        name: 'NA（ナトリウム）'
      },
      {
        name: 'K（カリウム）'
      },
      {
        name: 'Cl（クロール）'
      },
      {
        name: 'T-BIL（総ビリルビン）'
      },
      {
        name: 'D-BIL（直接ビリルビン）'
      },
      {
        name: 'LDH'
      },
      {
        name: 'ALP（アルカリフォスファターゼ）'
      },
      {
        name: 'Ch-E（コリンエステラーゼ）'
      },
      {
        name: 'CPK'
      },
      {
        name: 'AMY（アミラーゼ）'
      },
      {
        name: 'PT（プロトロンビン時間）'
      },
      {
        name: 'APTT（活性化部分トロンボプラスチン時間）'
      },
      {
        name: 'D・ダイマー'
      },
      {
        name: 'フェリチン'
      }
    ],
    data: REGISTERED
  },
  {
    id: '2',
    value: 'Blood lipit test',
    nihonName: '血中脂質検査',
    field: [
      {
        name: '中性脂肪'
      },
      {
        name: 'HDLコレステロール'
      },
      {
        name: 'LDLコレステロール'
      }
    ],
    data: REGISTERED
  },
  {
    id: '3',
    value: 'Liver function test',
    nihonName: '肝機能検査',
    field: [
      {
        name: '中性脂肪'
      },
      {
        name: 'HDLコレステロール'
      },
      {
        name: 'LDLコレステロール'
      }
    ],
    data: REGISTERED
  },
  {
    id: '4',
    value: 'Blood sugar test',
    nihonName: '血糖検査',
    field: [
      {
        name: '空腹時血糖'
      },
      {
        name: 'ヘモグロビンA1ｃ'
      }
    ],
    data: REGISTERED
  },
  {
    id: '5',
    value: 'Urinalysis',
    nihonName: '尿検査',
    field: [
      {
        name: '糖'
      },
      {
        name: '蛋白'
      }
    ],
    data: REGISTERED
  },
  {
    id: '6',
    value: 'Anemia test',
    nihonName: '貧血検査',
    field: [
      {
        name: '赤血球数'
      },
      {
        name: '血色素量'
      },
      {
        name: 'ヘマトクリット値'
      }
    ],
    data: REGISTERED
  },
  {
    id: '7',
    value: 'Renal function test',
    nihonName: '腎機能検査',
    field: [
      {
        name: '血清クレアチニン'
      },
      {
        name: 'eGFR'
      }
    ],
    data: REGISTERED
  },
  {
    id: '8',
    value: 'Electrocardiography',
    nihonName: '心電図検査',
    field: [
      {
        name: STATUS_FIELD
      }
    ],
    data: REGISTERED
  },
  {
    id: '9',
    value: 'Fundus examination',
    nihonName: '眼底検査',
    field: [
      {
        name: STATUS_FIELD
      }
    ],
    data: REGISTERED
  },
  {
    id: '10',
    value: 'Other',
    nihonName: 'その他',
    field: [
      {
        name: '既往歴'
      },
      {
        name: '服薬歴'
      },
      {
        name: '喫煙歴'
      },
      {
        name: '自覚症状'
      },
      {
        name: '他覚症状'
      }
    ],
    data: REGISTERED
  }
];
