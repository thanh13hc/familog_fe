export const SPUTUM_CYTOLOGY_ID = 'd04';
export const METABOLIC_SYNDROME_JUDMENT = [
  {
    value: 'CriteriaApplicable',
    text: '基準該当'
  },
  {
    value: 'ApplicableToReserveGroup',
    text: '予備群該当'
  },
  {
    value: 'NotApplicable',
    text: '非該当'
  }
];
export const DISEASE_TYPE = Object.freeze([
  {
    id: 'd01',
    name: 'periodontalDisease',
    realName: '歯周病疾患検診'
  },
  {
    id: 'd02',
    name: 'osteoporosis',
    realName: '骨粗鬆症検診'
  },
  {
    id: 'd03',
    name: 'gastricCancer',
    realName: '胃がん検診'
  },
  {
    id: 'd04',
    name: 'lungCancer',
    realName: '肺がん検診'
  },
  {
    id: 'd05',
    name: 'colorectalCancer',
    realName: '大腸がん検診'
  },
  {
    id: 'd06',
    name: 'cervicalCancer',
    realName: '子宮頚がん検診'
  },
  {
    id: 'd07',
    name: 'breastCancer',
    realName: '乳がん検診'
  }
]);
