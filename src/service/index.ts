import axios from 'axios';

const baseURL = 'http://localhost:8000/';

const $http = axios.create({
  baseURL
});

$http.defaults.headers.post['Content-Type'] = 'application/json';

export default { $http };
