export { default as ConfirmToDelete } from './ConfirmToDelete.vue';
export { default as EditPost } from './EditPost.vue';
export { default as NewPost } from './NewPost.vue';
export { default as Post } from './Post.vue';
export { default as PostsList } from './PostsList.vue';
export { default as SharingSetting } from './SharingSetting.vue';
