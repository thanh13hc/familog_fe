import { cloneDeep } from 'lodash-es';
import { POST_CATEGORY } from '@/constants';
import { UserType, PostType } from '@/types';
import { updateUser } from '@/utils';

export const initializePost = (
  loginID: string,
  loginUser: UserType,
  subCategory: string
) => {
  const post = {
    category: POST_CATEGORY.endOfLifePlanning.id,
    subCategory,
    title: 'ending notes',
    text: 'ending notes',
    detail: null,
    userID: loginID,
    user: loginUser,
    sharedID: null,
    photos: null
  };
  return post;
};

export const updateUserWithProfileData = async (
  post: PostType,
  user: UserType
) => {
  if (!post.detail) {
    return;
  }
  const userTobeUpdated = cloneDeep(user);
  // eslint-disable-next-line
  // @ts-ignore
  userTobeUpdated.surname = post.detail.attribute.data.surname.data;
  // eslint-disable-next-line
  // @ts-ignore
  userTobeUpdated.name = post.detail.attribute.data.name.data;
  // eslint-disable-next-line
  // @ts-ignore
  userTobeUpdated.birthday = post.detail.attribute.data.birthday.data;
  const userUpdated = await updateUser(userTobeUpdated.userID, userTobeUpdated);
  return userUpdated;
};
