import { UserType, PostType } from '@/types';
import { getPost, updatePost } from '@/utils';

export const updateProfilePost = async (user: UserType) => {
  if (!user.profileID) {
    return;
  }
  const post: PostType = await getPost(user.profileID);
  if (!post.detail) {
    return;
  }
  // eslint-disable-next-line
  // @ts-ignore
  post.detail.profile.subCategory.attribute.data.surname.data = user.surname;
  // eslint-disable-next-line
  // @ts-ignore
  post.detail.profile.subCategory.attribute.data.name.data = user.name;
  // eslint-disable-next-line
  // @ts-ignore
  post.detail.profile.subCategory.attribute.data.birthday.data = user.birthday;
  await updatePost(post.postID, post);
};
