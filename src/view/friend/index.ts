export { default as AddFriend } from './AddFriend.vue';
export { default as FriendHome } from './FriendHome.vue';
export { default as FriendsList } from './FriendsList.vue';
export { default as SearchFriend } from './SearchFriend.vue';
