export { default as RecordOfEachExam } from './RecordOfEachExam.vue';
export { default as Record } from './Record.vue';
export { default as Register } from './Register.vue';
export { default as Detail } from './Detail.vue';
export { default as ConfirmToDelete } from './ConfirmToDelete.vue';
export { default as EditHealthIssue } from './EditHealthIssue.vue';

