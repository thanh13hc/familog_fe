export { default as MedicalExaminationCalendar } from './MedicalExaminationCalendar.vue';
export { default as RegisterForMedicalExamination } from './MedicalRegister.vue';
export { default as MedicalExamDetail } from './MedicalExamDetail.vue';
export { default as MedicalEditForm } from './MedicalEditForm.vue';
export { default as ConfirmToDelete } from './MedicalExamDetail.vue';
