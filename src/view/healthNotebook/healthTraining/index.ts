export { default as HealthTraining } from './HealthTraining.vue';
export { default as Detail } from './Detail.vue';
export { default as Register } from './Register.vue';
export { default as ConfirmToDelete } from './ConfirmToDelete.vue';
export { default as HealthTrainingEdit } from './HealthTrainingEdit.vue';
