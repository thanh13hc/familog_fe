export { default as MedicalExaminationRecord } from './MedicalExaminationRecord.vue';
export { default as MedicalRecordDetail } from './MedicalRecordDetail.vue';
export { default as MedicalRecordRegisterForm } from './MedicalRecordRegisterForm.vue';
export { default as MedicalRecordEditForm } from './MedicalRecordEditForm.vue';
export { default as PreRegister } from './PreRegister.vue';
export { default as MedicalExamCategory } from './MedicalExamCategory.vue';
