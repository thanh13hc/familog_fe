export { default as PrimaryCareHospital } from './PrimaryCareHospital.vue';
export { default as PrimaryCareHospitalDetail } from './PrimaryCareHospitalDetail.vue';
export { default as PrimaryCareHospitalRegister } from './PrimaryCareHospitalRegister.vue';
export { default as PrimaryCareMedicalRegister } from './PrimaryCareMedicalRegister.vue';
export { default as PrimaryCareHospitalEdit } from './PrimaryCareHospitalEdit.vue';
export { default as PrimaryCareMedicalEdit } from './PrimaryCareMedicalEdit.vue';
export { default as PrimaryCareHospitalList } from './PrimaryCareHospitalList.vue';
export { default as ConfirmToDelete } from './ConfirmToDelete.vue';

