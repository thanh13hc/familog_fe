export { default as SpecificInsuranceGuidance } from './SpecificInsuranceGuidance.vue';
export { default as SpecificInsuranceGuidanceDetail } from './SpecificInsuranceGuidanceDetail.vue';
export { default as SpecificInsuranceGuidanceRegister } from './SpecificInsuranceGuidanceRegister.vue';
export { default as ConfirmToDelete } from './ConfirmToDelete.vue';
export { default as SpecificInsuranceGuidanceEdit } from './SpecificInsuranceGuidanceEdit.vue';
