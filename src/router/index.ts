import Vue from 'vue';
import VueRouter from 'vue-router';
import { Home, NotFound, CreateAccount, Login } from '@/view';
import { healthNotebook } from './healthNotebook';

//Vue.use(VueMoment);
Vue.use(VueRouter);

const routes = [
  ...healthNotebook,
  {
    path: '/',
    name: 'Top',
    component: Login
  },
  {
    path: '/login',
    name: 'Login',
    query: { next: '' },
    component: Login
  },
  {
    path: '/account/create',
    name: 'CreateAccount',
    query: { next: '' },
    component: CreateAccount
  },
  {
    path: '/account/edit',
    name: 'EditAccount',
    query: { next: '' },
    component: () => import('@/view/account/EditAccount.vue')
  },
  {
    path: '/home',
    name: 'Home',
    query: { auth: '' },
    component: Home
  },
  {
    path: '/friend',
    name: 'FriendHome',
    query: { auth: '' },
    component: () => import('@/view/friend/FriendHome.vue')
  },
  {
    path: '/friend/list',
    name: 'FriendsList',
    query: { next: '' },
    component: () => import('@/view/friend/FriendsList.vue')
  },
  {
    path: '/friend/search',
    name: 'SearchFriend',
    query: { next: '' },
    component: () => import('@/view/friend/SearchFriend.vue')
  },
  {
    path: '/friend/add',
    name: 'AddFriend',
    query: { next: '' },
    component: () => import('@/view/friend/AddFriend.vue')
  },
  {
    path: '/post/add',
    name: 'NewPost',
    query: { next: '' },
    component: () => import('@/view/post/NewPost.vue')
  },
  {
    path: '/post/category',
    name: 'SelectCategory',
    query: { next: '' },
    component: () => import('@/view/post/SelectCategory.vue')
  },
  {
    path: '/post/edit',
    name: 'EditPost',
    query: { next: '' },
    component: () => import('@/view/post/EditPost.vue')
  },
  {
    path: '/post/sharing',
    name: 'SharingSetting',
    query: { next: '' },
    component: () => import('@/view/post/SharingSetting.vue')
  },
  {
    path: '/posts',
    name: 'PostsList',
    query: { next: '' },
    component: () => import('@/view/post/PostsList.vue')
  },
  {
    path: '/post',
    name: 'Post',
    query: { next: '' },
    component: () => import('@/view/post/Post.vue')
  },
  {
    path: '/post/delete',
    name: 'ConfirmToDelete',
    query: { next: '' },
    component: () => import('@/view/post/ConfirmToDelete.vue')
  },
  {
    path: '/post/ending/category',
    name: 'EndingCategory',
    query: { next: '' },
    component: () => import('@/view/post/ending/EndingCategory.vue')
  },
  {
    path: '/post/ending/sub-category',
    name: 'EndingSubCategory',
    query: { next: '' },
    component: () => import('@/view/post/ending/EndingSubCategory.vue')
  },
  {
    path: '/post/ending/form',
    name: 'EndingNotesForm',
    query: { next: '' },
    component: () => import('@/view/post/ending/EndingNotesForm.vue')
  },
  {
    path: '/post/ending/sharing',
    name: 'SharingEndingSetting',
    query: { next: '' },
    component: () => import('@/view/post/ending/SharingEndingSetting.vue')
  },
  {
    path: '*',
    component: NotFound
  }
  // {
  //   path: "/about",
  //   name: "About",
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/About.vue")
  // }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;
