export const healthNotebook = [
  {
    path: '/health-notebook',
    name: 'HealthNotebook',
    query: { next: '' },
    component: () => import('@/view/healthNotebook/HealthNotebookCategory.vue')
  },
  {
    path: '/health-notebook/examination-calendar',
    name: 'ExaminationCalendar',
    component: () =>
      import(
        '@/view/healthNotebook/medicalExamCalendar/MedicalExamCalendar.vue'
      )
  },
  {
    path: '/health-notebook/examination-calendar/register',
    name: 'RegisterMedicalExamination',
    component: () =>
      import(
        '@/view/healthNotebook/medicalExamCalendar/MedicalRegisterForm.vue'
      )
  },
  {
    path: '/health-notebook/examination-calendar/delete',
    name: 'DeleteMedicalExamination',
    component: () =>
      import('@/view/healthNotebook/medicalExamCalendar/ConfirmToDelete.vue')
  },
  {
    path: '/health-notebook/examination-calendar/edit',
    name: 'EditMedicalExamination',
    component: () =>
      import('@/view/healthNotebook/medicalExamCalendar/MedicalEditForm.vue')
  },
  {
    path: '/health-notebook/examination-calendar/detail',
    name: 'MedicalExamDetail',
    component: () =>
      import('@/view/healthNotebook/medicalExamCalendar/MedicalExamDetail.vue')
  },

  // PrimaryCare Hospital
  {
    path: '/healthnotebook/primarycare-hopsital',
    name: 'PrimaryCareHospital',
    component: () =>
      import(
        '@/view/healthNotebook/primaryCareHospital/PrimaryCareHospital.vue'
      )
  },
  {
    path: '/healthnotebook/primarycare-hopsital/detail',
    name: 'PrimaryCareHospitalDetail',
    component: () =>
      import(
        '@/view/healthNotebook/primaryCareHospital/PrimaryCareHospitalDetail.vue'
      )
  },
  {
    path: '/health-notebook/primary-care-hospital/hospital-register',
    name: 'PrimaryCareHospitalRegister',
    component: () =>
      import(
        '@/view/healthNotebook/primaryCareHospital/PrimaryCareHospitalRegister.vue'
      )
  },
  {
    path: '/health-notebook/primary-care-hospital/medical-department-register',
    name: 'PrimaryCareMedicalRegister',
    component: () =>
      import(
        '@/view/healthNotebook/primaryCareHospital/PrimaryCareMedicalRegister.vue'
      )
  },
  {
    path: '/health-notebook/primary-care-hospital/hospital-edit',
    name: 'PrimaryCareHospitalEdit',
    component: () =>
      import(
        '@/view/healthNotebook/primaryCareHospital/PrimaryCareHospitalEdit.vue'
      )
  },
  {
    path: '/health-notebook/primary-care-hospital/medical-department-edit',
    name: 'PrimaryCareMedicalEdit',
    component: () =>
      import(
        '@/view/healthNotebook/primaryCareHospital/PrimaryCareMedicalEdit.vue'
      )
  },
  {
    path: '/health-notebook/primary-care-hospital/delete',
    name: 'DeleteHospital',
    component: () =>
      import('@/view/healthNotebook/primaryCareHospital/ConfirmToDelete.vue')
  },

  // Medical examination record
  {
    path: '/health-notebook/medical-examination-record',
    name: 'MedicalExaminationRecord',
    component: () =>
      import(
        '@/view/healthNotebook/bodyMeasurement/MedicalExaminationRecord.vue'
      )
  },
  {
    path:
      '/health-notebook/medical-examination-record/delete/:bodyMeasurementID',
    name: 'DeleteBodyMeasurement',
    component: () =>
      import('@/view/healthNotebook/bodyMeasurement/ConfirmToDelete.vue')
  },
  {
    path: '/health-notebook/medical-examination-record/edit/:bodyMeasurementID',
    name: 'EditBodyMeasurement',
    component: () =>
      import('@/view/healthNotebook/bodyMeasurement/EditBodyMeasurement.vue')
  },
  {
    path: '/health-notebook/medical-examination-pre-register',
    name: 'PreRegister',
    component: () =>
      import('@/view/healthNotebook/bodyMeasurement/PreRegister.vue')
  },
  {
    path: '/health-notebook/medical-examination-category/:bodyMeasurementID',
    name: 'MedicalExamCategory',
    component: () =>
      import('@/view/healthNotebook/bodyMeasurement/MedicalExamCategory.vue')
  },
  {
    path:
      '/health-notebook/medical-examination-register/:bodyMeasurementID/:id',
    name: 'MedicalRecordRegisterForm',
    component: () =>
      import(
        '@/view/healthNotebook/bodyMeasurement/MedicalRecordRegisterForm.vue'
      )
  },
  {
    path: '/health-notebook/medical-examination-edit/:bodyMeasurementID/:id',
    name: 'MedicalRecordEditForm',
    component: () =>
      import('@/view/healthNotebook/bodyMeasurement/MedicalRecordEditForm.vue')
  },
  {
    path: '/health-notebook/medical-examination-detail/:bodyMeasurementID/:id',
    name: 'MedicalRecordDetail',
    component: () =>
      import('@/view/healthNotebook/bodyMeasurement/MedicalRecordDetail.vue')
  },

  // Health training
  {
    path: '/healthnotebook/health-training',
    name: 'HealthTraining',
    component: () =>
      import('@/view/healthNotebook/healthTraining/HealthTraining.vue')
  },
  {
    path: '/health-notebook/health-training-detail',
    name: 'HealthTrainingDetail',
    component: () => import('@/view/healthNotebook/healthTraining/Detail.vue')
  },
  {
    path: '/health-notebook/health-training-register',
    name: 'HealthTrainingRegister',
    component: () => import('@/view/healthNotebook/healthTraining/Register.vue')
  },
  {
    path: '/healthnotebook/health-training/delete',
    name: 'HealthTrainingDelete',
    component: () =>
      import('@/view/healthNotebook/healthTraining/ConfirmToDelete.vue')
  },
  {
    path: '/healthnotebook/health-training/edit',
    name: 'HealthTrainingEdit',
    component: () =>
      import('@/view/healthNotebook/healthTraining/HealthTrainingEdit.vue')
  },

  // Record of exam
  {
    path: '/health-notebook/record-of-each-exam',
    name: 'RecordOfEachExam',
    component: () =>
      import('@/view/healthNotebook/healthIssue/RecordOfEachExam.vue')
  },
  {
    path: '/health-notebook/list-of-record/:id',
    name: 'EachExamRecordList',
    component: () => import('@/view/healthNotebook/healthIssue/Record.vue')
  },
  {
    path: '/health-notebook/register-new-examination/:id',
    name: 'RegisterOfEachExamRecord',
    component: () => import('@/view/healthNotebook/healthIssue/Register.vue')
  },
  {
    path: '/health-notebook/record-detail/:id/:healthIssueID',
    name: 'DetailOfEachExamRecord',
    component: () => import('@/view/healthNotebook/healthIssue/Detail.vue')
  },
  {
    path: '/health-notebook/record-detail/edit/:id/:healthIssueID',
    name: 'EditHealthIssueRecord',
    component: () =>
      import('@/view/healthNotebook/healthIssue/EditHealthIssue.vue')
  },
  {
    path: '/health-notebook/record-detail/delete/:id/:healthIssueID',
    name: 'DeleteHealthIssueRecord',
    component: () =>
      import('@/view/healthNotebook/healthIssue/ConfirmToDelete.vue')
  },

  // Specific insurance guidance
  {
    path: '/health-notebook/specific-insurance-guidance',
    name: 'SpecificInsuranceGuidance',
    component: () =>
      import(
        '@/view/healthNotebook/insuranceNote/SpecificInsuranceGuidance.vue'
      )
  },
  {
    path: '/health-notebook/specific-insurance-guidance/registration',
    name: 'SpecificInsuranceGuidanceRegister',
    component: () =>
      import(
        '@/view/healthNotebook/insuranceNote/SpecificInsuranceGuidanceRegister.vue'
      )
  },
  {
    path: '/health-notebook/specific-insurance-guidance/edit',
    name: 'InsuranceNoteEdit',
    component: () =>
      import(
        '@/view/healthNotebook/insuranceNote/SpecificInsuranceGuidanceEdit.vue'
      )
  },
  {
    path: '/health-notebook/specific-insurance-guidance/delete',
    name: 'DeleteInsuranceNote',
    component: () =>
      import('@/view/healthNotebook/insuranceNote/ConfirmToDelete.vue')
  },
  {
    path: '/health-notebook/specific-insurance-guidance/detail',
    name: 'SpecificInsuranceGuidanceDetail',
    component: () =>
      import(
        '@/view/healthNotebook/insuranceNote/SpecificInsuranceGuidanceDetail.vue'
      )
  }
];
