import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './storeVuex/index';
import Amplify, * as AmplifyModules from 'aws-amplify';
import Vuelidate from 'vuelidate';
import { AmplifyPlugin } from 'aws-amplify-vue';
import awsconfig from './aws-exports';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import { COUNTRY_CODE, POST_CATEGORY } from './constants';

Vue.config.productionTip = false;

Vue.use(AmplifyPlugin, AmplifyModules);
Vue.use(BootstrapVue);
Vue.use(Vuelidate);

Amplify.configure(awsconfig);

new Vue({
  data: {
    countryCode: COUNTRY_CODE,
    postCategory: POST_CATEGORY
  },
  router,
  store,
  render: h => h(App)
}).$mount('#app');
