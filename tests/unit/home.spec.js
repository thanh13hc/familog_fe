import Vue from 'vue';
import { shallowMount, mount, createLocalVue } from '@vue/test-utils';
import Home from '@/view/Home.vue';
import Login from '@/view/account/Login.vue';
import VueRouter from 'vue-router';

Vue.config.silent = true;
const localVue = createLocalVue();
localVue.use(VueRouter);

const routes = [{ path: '/', component: Login, name: 'Top' }];

const router = new VueRouter({
  routes
});

describe('Home.vue', () => {
  test('is a Vue instance', () => {
    const wrapper1 = shallowMount(Home, {
      localVue,
      router
    });

    expect(wrapper1.isVueInstance()).toBeTruthy();
  });
  test('renders correctly', () => {
    const wrapper2 = mount(Home, {
      localVue,
      router
    });
    expect(wrapper2.element).toMatchSnapshot();
  });
  test('renders props.msg when passed', () => {
    const msg = '投稿 0  ユーザーID：... 共有 0   ファミログを家族に紹介する';
    const wrapper3 = shallowMount(Home, {
      propsData: { msg },
      localVue,
      router
    });
    expect(wrapper3.text()).toBe(msg);
  });

  test('renders default message if not passed a prop', () => {
    const defaultMessage =
      '投稿 0  ユーザーID：... 共有 0   ファミログを家族に紹介する';
    const wrapper4 = shallowMount(Home, {
      localVue,
      router
    });
    expect(wrapper4.text()).toBe(defaultMessage);
  });
  test('user is empty by default', () => {
    const wrapper5 = mount(Home, {
      localVue,
      router
    });
    expect(wrapper5.vm.errorMessage).toEqual('');
  });
  test('postCounter has zero values by default', () => {
    const wrapper6 = mount(Home, {
      localVue,
      router
    });
    expect(wrapper6.vm.postCounter).toEqual({
      post: 0,
      share: 0
    });
  });
  test('posts is empty by default', () => {
    const wrapper7 = mount(Home, {
      localVue,
      router
    });
    expect(wrapper7.vm.posts).toEqual([]);
  });
  test('showID is empty by default', () => {
    const wrapper8 = mount(Home, {
      localVue,
      router
    });
    expect(wrapper8.vm.showID).toEqual('');
  });
  test('loginID is empty by default', () => {
    const wrapper9 = mount(Home, {
      localVue,
      router
    });
    expect(wrapper9.vm.loginID).toEqual('');
  });
});
