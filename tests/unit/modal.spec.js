import { mount, shallowMount } from '@vue/test-utils';
import Modal from '@/components/Modal.vue';
import Vue from 'vue';

Vue.config.silent = true;

describe('Modal.vue', () => {
  test('is a Vue instance', () => {
    const wrapper1 = mount(Modal);
    expect(wrapper1.isVueInstance()).toBeTruthy();
  });
  test('matches snapshot', () => {
    const wrapper2 = mount(Modal);
    expect(wrapper2.element).toMatchSnapshot();
  });
  test('renders props when passed', () => {
    const testBoolean = false;
    const testTitle = 'TestTitle';
    const testMessage = 'TestMessage';
    const wrapper3 = mount(Modal, {
      propsData: {
        isModalOpen: false,
        title: 'TestTitle',
        message: 'TestMessage'
      }
    });
    expect(wrapper3.props().isModalOpen).toBe(testBoolean);
    expect(wrapper3.props().title).toBe(testTitle);
    expect(wrapper3.props().message).toBe(testMessage);
  });
  test('renders default message if not passed a prop', () => {
    const defaultMessage = `エラー  Close`;
    const wrapper4 = mount(Modal);
    expect(wrapper4.text()).toBe(defaultMessage);
  });
  test('renders a div', () => {
    const testBoolean = false;
    const testTitle = 'Test Title';
    const testMessage = 'Test Message';
    const wrapper5 = shallowMount(Modal, {
      propsData: {
        isModalOpen: false,
        title: 'Test Title',
        message: 'Test Message'
      }
    });
    expect(wrapper5.props().isModalOpen).toBe(testBoolean);
    expect(wrapper5.props().title).toBe(testTitle);
    expect(wrapper5.props().message).toBe(testMessage);
  });
});
