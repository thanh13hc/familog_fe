import { shallowMount, mount } from '@vue/test-utils';
import EditAccount from '@/view/account/EditAccount.vue';
import Vue from 'vue';
import { store } from '@/store';

Vue.config.silent = true;

describe('EditAccount.vue', () => {
  const store2 = store;
  store2.state.loginUser.birthday = '05 October 2011 14:48';
  test('is a Vue instance', () => {
    const wrapper1 = mount(EditAccount, {
      store2
    });
    expect(wrapper1.isVueInstance()).toBeTruthy();
  });
  test('renders correctly', () => {
    const wrapper2 = mount(EditAccount);
    expect(wrapper2.element).toMatchSnapshot();
  });
  test('renders props.msg when passed', () => {
    const msg = `プロフィール画像 
            ファイルを選択
             選択されていません カバー画像 
            ファイルを選択
             選択されていません`;
    const wrapper3 = shallowMount(EditAccount, {
      propsData: { msg }
    });
    expect(wrapper3.text()).toBe(msg);
  });
  test('renders default message if not passed a prop', () => {
    const defaultMessage = `プロフィール画像 
            ファイルを選択
             選択されていません カバー画像 
            ファイルを選択
             選択されていません`;
    const wrapper4 = shallowMount(EditAccount);
    expect(wrapper4.text()).toBe(defaultMessage);
  });
});
