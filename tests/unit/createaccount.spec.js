import { mount } from '@vue/test-utils';
import CreateAccount from '@/view/account/CreateAccount.vue';
import Vue from 'vue';

Vue.config.silent = true;

describe('CreateAccount.vue', () => {
  test('is a Vue instance', () => {
    const wrapper1 = mount(CreateAccount);
    expect(wrapper1.isVueInstance()).toBeTruthy();
  });
  test('renders correctly', () => {
    const wrapper2 = mount(CreateAccount);
    expect(wrapper2.element).toMatchSnapshot();
  });
  // test('renders props.msg when passed', () => {
  //   const msg = '投稿 0  共有 0 ユーザーID：           ファミログを家族に紹介する';
  //   const wrapper3 = shallowMount(CreateAccount, {
  //     propsData: { msg },
  //   });
  //   expect(wrapper3.text()).toBe(msg);
  // });

  // test('renders default message if not passed a prop', () => {
  //   const defaultMessage =
  //   `アカウント作成

  //   姓 名   生年月日  電話番号 日本アイスランドアイルランドアゼルバイジャンアフガニスタンアメリカ／カナダアラブ首長国連邦アルジェリアアルゼンチンイギリスイタリアインドインドネシアエジプトエチオピアオーストラリアオーストリアオランダカメルーンキューバギリシャキリバスケニアコートジボワールコロンビアサウジアラビアシンガポールスイススウェーデンスーダンスペインソロモン諸島タイチュニジアチリツバルデンマークドイツニューカレドニアニュージーランドネパールパプアニューギニアフィジーフィリピンフィンランドブラジルフランスベトナムペルーポルトガルマレーシアメキシコモロッコモンゴルロシア香港台湾大韓民国中央アフリカ中華人民共和国南アフリカ  パスワード   プロフィール画像
  //               ファイル選択
  //                   選択されていません カバー画像
  //               ファイル選択
  //                   選択されていません`;
  //   const wrapper4 = shallowMount(CreateAccount);
  //   expect(wrapper4.text()).toBe(defaultMessage);
  // });
  // test('user is empty by default', () => {
  //   const wrapper5 = mount(CreateAccount);
  //   expect(wrapper5.vm).toEqual({
  //     password: '',
  //     confirmCode: '',
  //     userInfo: {
  //       userID: '',
  //       surname: '',
  //       name: '',
  //       birthday: '',
  //       countryCode: '',
  //       phoneNumber: '',
  //       profile: {
  //         showFileName: '',
  //         base64: '',
  //         filename: '',
  //       },
  //       cover: {
  //         showFileName: '',
  //         base64: '',
  //         filename: '',
  //       },
  //     },
  //     selectedCountry: '+81',
  //     countries: countryCode,
  //     errorMessage: '',
  //   });
  // });
});
