import { mount } from '@vue/test-utils';
import SearchFriend from '../../src/view/friend/SearchFriend.vue';
import Vue from 'vue';

Vue.config.silent = true;

describe('SearchFriend.vue', () => {
  test('is a Vue instance', () => {
    const wrapper1 = mount(SearchFriend);
    expect(wrapper1.isVueInstance()).toBeTruthy();
  });
  test('renders correctly', () => {
    const wrapper2 = mount(SearchFriend);
    expect(wrapper2.element).toMatchSnapshot();
  });
});
