import { mount } from '@vue/test-utils';
import FriendsList from '../../src/view/friend/FriendsList.vue';
import Vue from 'vue';

Vue.config.silent = true;

describe('FriendsList.vue', () => {
  test('is a Vue instance', () => {
    const wrapper1 = mount(FriendsList);
    expect(wrapper1.isVueInstance()).toBeTruthy();
  });
  test('renders correctly', () => {
    const wrapper2 = mount(FriendsList);
    expect(wrapper2.element).toMatchSnapshot();
  });
});
