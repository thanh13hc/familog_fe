import Vue from 'vue';
import { shallowMount, mount } from '@vue/test-utils';
import Login from '@/view/account/Login.vue';
import { COUNTRY_CODE } from '@/constants/countryCode';

Vue.config.silent = true;

describe('Login.vue', () => {
  test('is a Vue instance', () => {
    const wrapper1 = mount(Login);
    expect(wrapper1.isVueInstance()).toBeTruthy();
  });
  test('renders correctly', () => {
    const wrapper2 = mount(Login);
    expect(wrapper2.element).toMatchSnapshot();
  });
  test('renders props.msg when passed', () => {
    const msg = 'アカウントを作成';
    const wrapper3 = shallowMount(Login, {
      propsData: { msg }
    });
    expect(wrapper3.text()).toBe(msg);
  });

  test('renders default message if not passed a prop', () => {
    const defaultMessage = 'アカウントを作成';
    const wrapper4 = shallowMount(Login);
    expect(wrapper4.text()).toBe(defaultMessage);
  });
  test('formUserInfo is empty by default', () => {
    const wrapper5 = mount(Login);
    expect(wrapper5.vm.formUserInfo).toEqual({
      phoneNumber: '',
      awsUsername: '',
      password: ''
    });
  });
  test('selectedCountry is +81 by default', () => {
    const wrapper6 = mount(Login);
    expect(wrapper6.vm.selectedCountry).toBe('+81');
  });
  test('countries is countryCode by default', () => {
    const wrapper7 = mount(Login);
    expect(wrapper7.vm.countryCode).toBe(COUNTRY_CODE);
  });
  test('errorMessage is empty by default', () => {
    const wrapper7 = mount(Login);
    expect(wrapper7.vm.errorMessage).toBe('');
  });
  test('check select country click html element', () => {
    const wrapper8 = mount(Login);
    const clickselectCountry = wrapper8.findAll('.form-input');
    expect(clickselectCountry.at(0).is('b-form-select')).toBe(true);
  });
  test('check phone number click html element', () => {
    const wrapper9 = mount(Login);
    const clickphoneNumber = wrapper9.find("[type='tel']");
    expect(clickphoneNumber.is('b-form-input')).toBe(true);
  });
  test('check password click html element', () => {
    const wrapper10 = mount(Login);
    const clickpassword = wrapper10.find("[placeholder='パスワード']");
    expect(clickpassword.is('b-form-input')).toBe(true);
  });
  test('check signin account click html element', () => {
    const wrapper11 = mount(Login);
    const clickSignin = wrapper11.find("[type='submit']");
    expect(clickSignin.is('button')).toBe(true);
  });
  test('check make account click html element', () => {
    const wrapper12 = mount(Login);
    const clickMakeAccount = wrapper12.find('button.button-create-account');
    expect(clickMakeAccount.is('button')).toBe(true);
  });
});
